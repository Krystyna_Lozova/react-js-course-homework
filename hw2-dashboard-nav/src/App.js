import React from 'react';
import NavSidebar from './components/nav-sidebar';
import Main from './components/main';

function App() {
  return (
    <div className="App">
      <NavSidebar />
      <Main />
    </div>
  );
}

export default App;
