import React from "react";

import IconWrapper from "../icon-wrapper";

import "./index.css";

const NavItem = ({url, name, icon}) => {
    return (
        <li className="nav-item">
          <a href={url}>
            <IconWrapper src={icon} alt={name} />
            <span>{name}</span>
          </a>
        </li>
    )
}

export default NavItem;
