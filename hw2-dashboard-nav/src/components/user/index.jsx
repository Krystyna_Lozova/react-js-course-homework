import React from "react";

import Avatar from "./../avatar";

import "./index.css";

const User = () => {
  return (
    <div className="user">
      <Avatar />
      <div className="user-info">
        <p className="text-bold">AnimatedFred</p>
        <p>animated@demo.com</p>
      </div>
    </div>
  );
};

export default User;
