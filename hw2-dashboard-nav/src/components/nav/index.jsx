import React from "react";

import { topNavItems, bottomNavItems } from "../data/data";

import Search from "../search";
import NavItem from "../nav-item";
import ThemeCheckboxWrapper from './../theme-checkbox-wrapper';

const Nav = () => {
  return (
    <nav className="nav">
      <Search />
      <div className="nav-top-block">
        <ul>
          {topNavItems.map(({ name, icon, url }, i) => {
            return <NavItem name={name} icon={icon} url={url} key={i + "-nav-top-block"} />;
          })}
        </ul>
      </div>

      <div className="nav-bottom-block">
        <ul className="m-b-0">
          {bottomNavItems.map(({ name, icon, url }, i) => {
            return <NavItem name={name} icon={icon} url={url} key={i + "-nav-bottom-block"} />;
          })}
        </ul>
        <ThemeCheckboxWrapper />
      </div>
    </nav>
  );
};

export default Nav;
