import React from "react";

import "./index.css"

const IconWrapper = ({src, alt}) => {
    return (
        <div className="icon-wrapper">
            <img src={src} alt={alt} width="24" />
        </div>
    )
}

export default IconWrapper;
