import React from "react";

import IconWrapper from "../icon-wrapper";

import searchIcon from "../../assets/images/icons/search.svg";

import "./index.css";

const Search = () => {
  return (
    <form className="search">
      <label htmlFor="search"></label>
      <input type="search" id="search" name="search" placeholder="Search..." />
      <IconWrapper  src={searchIcon} alt="search" />
    </form>
  );
};

export default Search;
