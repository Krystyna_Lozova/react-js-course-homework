import React from "react";

import MoonColoredIcon from "../../assets/images/icons/moon-colored.svg";
import SunColoredIcon from "../../assets/images/icons/sun-colored.svg";

import "./index.css";

const ThemeCheckbox = () => {
  return (
    <div className="theme-checkbox">
      <input type="checkbox" className="checkbox" id="toggleTheme" />
      <label htmlFor="toggleTheme" className="label">
        <img
          src={SunColoredIcon}
          title="light theme checked"
          alt=""
          className="light-theme-icon"
        />
        <img
          src={MoonColoredIcon}
          title="dark theme checked"
          alt=""
          className="dark-theme-icon"
        />
      </label>
    </div>
  );
};

export default ThemeCheckbox;
