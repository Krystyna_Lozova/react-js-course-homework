import React from "react";

import User from "../user";
import Nav from "../nav";
import IconWrapper from "../icon-wrapper";

import ArrowInCircle from "../../assets/images/icons/arrow-in-circle.svg";

import "./index.css";

const NavSidebar = () => {
  return (
    <div className="nav-sidebar">
      <div className="nav-sidebar-icon">
        <IconWrapper src={ArrowInCircle} />
      </div>
      <div className="nav-sidebar-content">
        <User />
        <Nav />
      </div>
    </div>
  );
};

export default NavSidebar;
