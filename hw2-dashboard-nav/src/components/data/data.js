export const topNavItems = [
    {
        name: "Dashboard",
        url: "/",
        icon: "./assets/images/icons/home.svg"

    },
    {
        name: "Revenue",
        url: "/",
        icon: "./assets/images/icons/revenue.svg"

    },
    {
        name: "Notifications",
        url: "/",
        icon: "./assets/images/icons/notifications.svg"

    },
    {
        name: "Analytics",
        url: "/",
        icon: "./assets/images/icons/analytics.svg"

    },
    {
        name: "Inventory",
        url: "/",
        icon: "./assets/images/icons/inventory.svg"

    },
]

export const bottomNavItems = [
    {
        name: "Logout",
        url: "/",
        icon: "./assets/images/icons/logout.svg"

    },
]