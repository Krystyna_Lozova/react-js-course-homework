import React from "react";

import IconWrapper from "../icon-wrapper";

import UserAvatar from "../../assets/images/icons/avatar.svg"

import "./index.css";

const Avatar = () => {
    return (
        <div className="avatar">
            <IconWrapper src={UserAvatar} alt="Animated Fred Avatar" />
        </div>
    )
}

export default Avatar;
