import React from "react";

import IconWrapper from "../icon-wrapper";
import ThemeCheckbox from "../theme-checkbox";

import SunIcon from "../../assets/images/icons/sun.svg";

import "./index.css";

const ThemeCheckboxWrapper = () => {
  return (
    <div className="theme-checkbox-wrapper">
      <div className="theme-checkbox-info">
        <IconWrapper src={SunIcon} alt="Sun" />
        <span>Light mode</span>
      </div>
      <ThemeCheckbox />
    </div>
  );
};

export default ThemeCheckboxWrapper;
