import React, { Component } from 'react';

import { Row, Col, Card, Image, Typography, Tag, Rate, Button, Result, Spin } from "antd";

import styles from "./ProductList.module.scss";

const { Meta } = Card;
const { Text } = Typography;


class ProductList extends Component {

    state = {
        error: null,
        isLoading: true,
        items: [],
    }

    // fetch data
    componentDidMount() {
        fetch("https://fakestoreapi.com/products")
            .then(res => res.json())
            .then(result => {
                this.setState(prevState => {
                    return {
                        ...prevState,
                        isLoading: false,
                        items: result
                    }
                })
            },
                error => {
                    this.setState(prevState => {
                        return {
                            ...prevState,
                            error: error,
                            isLoading: false
                        }
                    })
                }
            )
    }


    render() {
        const { error, isLoading, items } = this.state;

        // show an Error Message if the data was uploaded with an error
        if (error) {
            return (
                <Result
                    title="Sorry, something went wrong."
                    subTitle={"Error: " + error.message}
                />
            )
        }

        // show the spinner while loading data
        else if (isLoading) {
            return (
                <Spin tip="Loading" size="large">
                    <div className="content" />
                </Spin>
            )
        }

        // display product cards after successful data upload
        else {
            return (
                <Row gutter={24}
                    className={styles.CardList}
                >

                    {items.map(item => {
                        // assign a class to the Stock Tag depending on the stock balance
                        let stockValue = "";

                        if (item.rating.count >= 400) {
                            stockValue = "success"
                        } else if (item.rating.count > 200 &&  item.rating.count < 400) {
                            stockValue = "warning"
                        } else {
                            stockValue = "danger"
                        }

                        return (
                            <Col key={item.id} sm={24} md={12} xl={8} xxl={6} style={{ display: 'flex' }}>
                                <Card
                                    className={styles.Card}
                                    bodyStyle={{
                                        padding: "0",
                                        display: "flex",
                                        flexDirection: "column",
                                        height: "100%"
                                    }}
                                    style={{ flex: 1 }}
                                >
                                    <div className={styles.CardThumb}>
                                        <Image
                                            alt={item.title}
                                            src={item.image}
                                        />
                                    </div>
                                    <div className={styles.CardContent + " d-flex-col"}>
                                        <div className={styles.CardMeta}>
                                            <Tag className={styles.CardCat}>{item.category}</Tag>
                                            <Meta
                                                title={item.title}
                                                description={item.description}
                                            />
                                        </div>
                                        <div className={styles.CardInfo + " m-t-auto p-t-20"}>
                                            <Rate allowHalf disabled defaultValue={item.rating.rate} />
                                            <Tag type="secondary" className={stockValue + " m-t-10"}>In Stock: {item.rating.count} (items)</Tag>
                                            <div className={styles.CardPrice + " m-t-20 m-b-20"}>
                                                <Text className={styles.CardPrice + " animate-charcter"}>{item.price} UAH</Text>
                                            </div>
                                            <Button type="primary" block className={styles.CardBtn}>Buy</Button>
                                        </div>
                                    </div>
                                </Card>
                            </Col>
                        )
                    }
                    )}
                </Row>
            );
        }
    }
}

export default ProductList;
