import { Typography } from "antd";
import ProductList from "./ProductList";

import styles from "./App.module.scss";

const { Title } = Typography;




const App = () => {
    return (
        <div className={styles.App}>
            <Title className={styles.Title}>Our Products</Title>
            <ProductList />
        </div>
    )
}

export default App;