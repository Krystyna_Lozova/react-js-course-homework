import React from "react";
import ReactDOM from "react-dom";
import App from "./components/App";

// Ant Design styles file
import 'antd/dist/reset.css';
// our custom styles
import "./styles/styles.scss"


ReactDOM.render(<App />, document.querySelector("#root"));
