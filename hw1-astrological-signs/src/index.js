import React from "react";
import ReactDOM  from "react-dom";

import "./App.css";

// import images
const imageNames = [
    "sign_aquarius.png",
    "sign_aries.png",
    "sign_cancer.png",
    "sign_capricorn.png",
    "sign_gemini.png",
    "sign_leo.png",
    "sign_libra.png",
    "sign_pisces.png",
    "sign_sagittarius.png",
    "sign_scorpio.png",
    "sign_taurus.png",
    "sign_virgo.png"
];

const images = {};

imageNames.forEach(name => {
    images[name] = require(`../public/assets/images/${name}`);
})


function App () {
    return (
        <div className="container">
            <h1>Astrological signs</h1>
            <table>
                <thead>
                    <tr>
                    <th scope="col">№</th>
                    <th scope="col">Name</th>
                    <th scope="col">Dates</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>1</td>
                        <td>
                            <img 
                                alt="Aries"
                                width="24"
                                src={images["sign_aries.png"]} />
                            <span>Aries</span>
                        </td>
                        <td><span>March 21 – April 19</span></td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>
                            <img 
                                alt="Taurus"
                                width="24" 
                                src={images["sign_taurus.png"]} />
                            <span>Taurus</span>
                        </td>
                        <td><span>April 20 – May 20</span></td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>
                            <img 
                                alt="Gemini"
                                width="24" 
                                src={images["sign_gemini.png"]} />
                            <span>Gemini</span>
                        </td>
                        <td><span>May 21 – June 21</span></td>
                    </tr>
                    <tr>
                        <td>4</td>
                        <td>
                            <img 
                                alt="Cancer"
                                width="24" 
                                src={images["sign_cancer.png"]} />
                            <span>Cancer</span>
                        </td>
                        <td><span>June 22 – July 22</span></td>
                    </tr>
                    <tr>
                        <td>5</td>
                        <td>
                            <img 
                                alt="Leo"
                                width="24" 
                                src={images["sign_leo.png"]} />
                            <span>Leo</span>
                        </td>
                        <td><span>July 23 – August 22</span></td>
                    </tr>
                    <tr>
                        <td>6</td>
                        <td>
                            <img 
                                alt="Virgo"
                                width="24"
                                src={images["sign_virgo.png"]} />
                            <span>Virgo</span>
                        </td>
                        <td><span>August 23 – September 22</span></td>
                    </tr>
                    <tr>
                        <td>7</td>
                        <td>
                            <img 
                                alt="Libra"
                                width="24"
                                src={images["sign_libra.png"]} />
                            <span>Libra</span>
                        </td>
                        <td><span>September 23 – October 22</span></td>
                    </tr>
                    <tr>
                        <td>8</td>
                        <td>
                            <img 
                                alt="Scorpio"
                                width="24"
                                src={images["sign_scorpio.png"]} />
                            <span>Scorpio</span>
                        </td>
                        <td><span>October 23 – November 22</span></td>
                    </tr>
                    <tr>
                        <td>9</td>
                        <td>
                            <img 
                                alt="Sagittarius"
                                width="24"
                                src={images["sign_sagittarius.png"]} />
                            <span>Sagittarius</span>
                        </td>
                        <td><span>November 23 – December 21</span></td>
                    </tr>
                    <tr>
                        <td>10</td>
                        <td>
                            <img 
                                alt="Capricorn"
                                width="24"
                                src={images["sign_capricorn.png"]} />
                            <span>Capricorn</span>
                        </td>
                        <td><span>December 22 – January 19</span></td>
                    </tr>
                    <tr>
                        <td>11</td>
                        <td>
                            <img 
                                alt="Aquarius"
                                width="24"
                                src={images["sign_aquarius.png"]} />
                            <span>Aquarius</span>
                        </td>
                        <td><span>January 20 – February 18</span></td>
                    </tr>
                    <tr>
                        <td>12</td>
                        <td>
                            <img 
                                alt="Pisces"
                                width="24"
                                src={images["sign_pisces.png"]} />
                            <span>Pisces</span>
                        </td>
                        <td><span>February 19 – March 20</span></td>
                    </tr>
                </tbody>
                </table>
        </div>
    )
}

ReactDOM.render(<App />, document.querySelector("#app"));
