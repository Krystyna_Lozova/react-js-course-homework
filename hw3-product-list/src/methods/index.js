export const validate = (val, pattern) => {
    return pattern.test(val);
}