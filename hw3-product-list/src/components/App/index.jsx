import { Component } from "react";
import AddProductForm from "../AddProductForm";
import ProductItem from "../ProductItem";
import EditProductForm from "../EditProductForm";
import Button from "../Button";

import { validate } from "../../methods";


class App extends Component {
    state = {
        isAppActive: false,
        products: [],
        formsInputs: {
            addInputVal: "",
            editInputVal: ""
        }
    }

    // when the user clicks on the "Add Product +" button
    handleActivateApp = () => {
        this.setState(state => {
            return {
                ...state,
                isAppActive: !state.isAppActive,
            }
        })
    }

    // when the user clicks the "Save" button after filling out the "Add Product Form" input field
    handleAddItem = (newProduct) => {
        if (newProduct !== "") {
            this.setState(state => {
                return {
                    ...state,
                    products: [
                        ...state.products,
                        {
                            id: Date.now(),
                            name: newProduct,
                            isCompleted: false,
                            isEdited: false
                        }
                    ],
                    formsInputs: {
                        addInputVal: "",
                        editInputVal: ""
                    }
                }
            })
        }
    }

    // when the user clicks on the "Edit" product button
    handleEditItem = (id) => {
        let productsList = this.state.products;
        let editedProductName = "";

        let updatedProductsList = productsList.map(item => {
            if (item.id === id) {
                return {
                    ...item,
                    isEdited: !item.isEdited,
                }
            } else {
                return {
                    ...item
                }
            }
        })

        updatedProductsList.filter(item => {
            return item.isEdited ? editedProductName = item.name : null
        })

        this.setState(state => {
            return {
                ...state,
                products: updatedProductsList,
                formsInputs: {
                    ...state.formsInputs,
                    editInputVal: editedProductName
                }
            }
        })
    }


    // when the user clicks on the "Save" product button after editing the product (into the Edit Product Form input field)
    handleUpdateItem = (value, id) => {
        let productsList = this.state.products

        let updatedList = productsList.map(item => {
            if (item.id === id) {
                return {
                    ...item,
                    name: value,
                    isEdited: !item.isEdited
                }
            } else {
                return {
                    ...item
                }
            }
        })

        this.setState(state => {
            return {
                ...state,
                products: updatedList,
                formsInputs: {
                    ...state.formsInputs,
                    editInputVal: ""
                }
            }
        })
    }

    // when the user clicks on the "Delete" product button
    handleDeleteItem = (id) => {
        const filteredList = this.state.products.filter(item => item.id !== id)

        this.setState(state => {
            return {
                ...state,
                products: filteredList
            }
        })
    }

    // when the user clicks on the "Complete" product button
    handleCompleteItem = (id) => {
        let productList = this.state.products;

        let updatedProductList = productList.map(item => {
            if (item.id === id) {
                return {
                    ...item,
                    isCompleted: !item.isCompleted
                }
            } else {
                return {
                    ...item
                }
            }
        })

        this.setState(state => {
            return {
                ...state,
                products: updatedProductList
            }
        })
    }

    // when the user is typing into the "Add Product From" & "Edit Product Form" input fields
    handleInputChange = (e, inputName) => {
        let inputVal = e.target.value;

        if (validate(inputName, /addInput/)) {
            this.setState(state => {
                return {
                    ...state,
                    formsInputs: {
                        ...this.state.formsInputs,
                        addInputVal: inputVal,
                    }
                }
            })
        } else if (validate(inputName, /editInput/)) {
            this.setState(state => {
                return {
                    ...state,
                    formsInputs: {
                        ...this.state.formsInputs,
                        editInputVal: inputVal,
                    }
                }
            })
        }
    }


    render() {
        return (
            <div className="app">
                <div className={this.state.isAppActive ? "app-header disabled" : "app-header"}>
                    <h1>Products</h1>
                    <Button
                        iconClass="fa-solid fa-plus"
                        text="Add Product"
                        eventFn={this.handleActivateApp}
                    />

                </div>
                <div className={this.state.isAppActive ? "app-body active" : "app-body"}>
                    <AddProductForm
                        id="add-form"
                        inputName="addInput"
                        inputValue={this.state.formsInputs.addInputVal}
                        addItem={this.handleAddItem}
                        changeInput={this.handleInputChange}
                    />

                    <div className="app-content">
                        <ul className="product-list">
                            {
                                this.state.products.map((product, index) => (
                                    product.isEdited ? (
                                        <li className="form-item" key={product.id}>
                                            <EditProductForm
                                                id="edit-form"
                                                productId={product.id}
                                                inputName="editInput"
                                                inputValue={this.state.formsInputs.editInputVal}
                                                changeInput={this.handleInputChange}
                                                updateItem={this.handleUpdateItem}
                                            />
                                        </li>
                                    ) : (
                                        <ProductItem
                                            key={product.id}
                                            id={product.id}
                                            name={product.name}
                                            isCompleted={product.isCompleted}
                                            editItem={this.handleEditItem}
                                            deleteItem={this.handleDeleteItem}
                                            completeItem={this.handleCompleteItem}
                                        />
                                    )
                                )
                                )
                            }
                        </ul>
                    </div>
                </div>
            </div>
        )
    }
}

export default App;
