const Button = ({ value, btnClassNames, iconClass, text, eventFn }) => {
    return (
        < button
            type="button"
            value={value}
            className={btnClassNames ? btnClassNames + " btn" : "btn"}
            onClick={ () => eventFn() }
        >
            {text !== "" ? <span>{text}</span> : null}
            {iconClass !== "" ? <i className={iconClass}></i> : null}
        </button >
    )
}

export default Button;
