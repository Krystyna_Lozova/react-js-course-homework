import Button from "../Button";

const ProductItem = ({ id, name, isCompleted, editItem, deleteItem, completeItem }) => {

    const editItemFn = () => {
        editItem(id)
    }

    const deleteItemFn = () => {
        deleteItem(id)
    }

    const checkItemFn = () => {
        completeItem(id)
    }

    return (
        <li>
            <p className={isCompleted ? "completed" : ""}>
                {name}
            </p>
            <div className="btns">
                <Button
                    value="edit"
                    btnClassNames={"btn--icon"}
                    iconClass="fa-solid fa-pen-to-square icon"
                    text=""
                    eventFn={editItemFn}
                />
                <Button
                    value="delete"
                    btnClassNames={"btn--icon"}
                    iconClass="fa-solid fa-trash icon"
                    text=""
                    eventFn={deleteItemFn}
                />
                <Button
                    value="check"
                    btnClassNames={"btn--icon"}
                    iconClass="fa-solid fa-square-check icon"
                    text=""
                    eventFn={checkItemFn}
                />
            </div>
        </li>
    )
}

export default ProductItem;
