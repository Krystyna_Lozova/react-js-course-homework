import Button from "../Button";
import Input from "../Input";

const AddProductForm = ({ id, inputName, inputValue, addItem, changeInput }) => {
  const addItemFn = () => {
    addItem(inputValue);
  }

  return (

    <div className="app-bar">
      <form
        action=""
        id={id} className="add-form form"
        onSubmit={(e) => {
          e.preventDefault();
          addItemFn();
        }}>

        <Input
          name={inputName}
          placeholder="type a product to bye"
          value={inputValue}
          eventFn={changeInput}
        />

        <Button
          value="save"
          btnClassNames={"btn--icon btn--square"}
          iconClass="fa-solid fa-floppy-disk icon"
          text=""
          eventFn={addItemFn}
        />

      </form>
    </div>

  )
}

export default AddProductForm;
