import Button from "../Button";
import Input from "../Input";

const EditProductForm = ({ id, productId, inputName, inputValue, changeInput, updateItem }) => {
    const updateItemFn = () => {
        updateItem(inputValue, productId);
    }

    return (

        <form action=""
            id={id}
            className="edit-form form"
            onSubmit={e => {
                e.preventDefault();
                updateItemFn();
            }}
        >
            <Input
                name={inputName}
                placeholder="update the product"
                value={inputValue}
                eventFn={changeInput}
            />
            <Button
                value="save"
                btnClassNames={"btn--icon"}
                iconClass="fa-solid fa-floppy-disk icon"
                text=""
                eventFn={updateItemFn}
            />
        </form>

    )
}

export default EditProductForm;
