const Input = ({ name, placeholder, value, eventFn }) => {
    return (
        <>
            <input 
                type="text" 
                name={name}
                placeholder={placeholder} 
                value={value}
                onChange={ (e) => eventFn(e, name)} 
            />
        </>
    )
}

export default Input;
